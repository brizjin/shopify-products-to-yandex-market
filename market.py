# -*- coding: windows-1251 -*-
import sublime, sublime_plugin
import os
#import urllib2
import urllib,json
import datetime
import xml.dom.minidom as xml


OUT_DIR 		= "/Users/ivan/Downloads/"
SHOP_NAME		= "set-and-app"
API_KEY			= "ea2e959b3400c2e48cb71bf1c45ffc5c"
API_PASSWORD 	= "50e0f99fd19351f06031c0238321e581"

market_dir = os.path.dirname(os.path.abspath(__file__))
is_from_chache = 0

class shopifyShop(object):


	def __init__(self):

		self.name         	= SHOP_NAME
		self.api_key      	= API_KEY
		self.api_password 	= API_PASSWORD
		self.url_template	= "https://%s:%s@%s.myshopify.com/admin/" % (API_KEY,API_PASSWORD,SHOP_NAME)
		#self.products_list	= None

	def load_shopify_json(self, address):
		#response = urllib.urlopen(address)

		if is_from_chache:
			if os.path.exists(os.path.join(market_dir,address)):
				return json.load(open(os.path.join(market_dir,address),'r'))

		response = urllib.urlopen(self.url_template + address)
		if( response.getcode() >= 400 ):
			sublime.set_timeout(lambda: sublime.error_message("Shopify authentication failed."), 0)
			#open_stores_file()
			raise Exception("Shopify authentication failed")

		res = json.load(response)

		#if is_from_chache:
		# f = open(os.path.join(OUT_DIR,address),'w+')
		# f.write(json.dumps(res))
		# f.close()
		
		return res

	# def products(self):
	# 	#return self.cache(self.load_shopify_json("products.json")["products"],"products_cache")
	# 	return self.get("products")

	# def custom_collections(self):
	# 	return self.load_shopify_json("custom_collections.json")["custom_collections"]
	
	def get(self,code):
		return self.cache(self.load_shopify_json(code + ".json")[code],code + "_cache")
	
	def product_types(self):		
		prod_types = enumerate(sorted([p["product_type"] for p in self.get("products")]))
		prod_types_distinct = dict()
		for i,v in prod_types:
			prod_types_distinct[v] = str(i)
		return prod_types_distinct

	def cache(self,obj,name):
		if hasattr(self, name):
			return getattr(self,name)
		setattr(self,name,obj)
		return obj

class xmlElement(object):
	def __init__(self,doc,tag_name,text=None,**attributes):
		self.doc = doc
		self.elem = self.createElement(tag_name,text,**attributes)
	def appendChild(self,tag_name,text=None,**attributes):
		#elem = self.createElement(tag_name,**attributes)
		xelem = xmlElement(self.doc,tag_name,text,**attributes)
		self.elem.appendChild(xelem.elem)
		return xelem
	def createElement(self,tag_name,text=None,**attributes):
		if text:
			#print "text",text
			elem = self.doc.createElement(tag_name)
			#elem.nodeValue = text

			elem.appendChild(self.doc.createTextNode(text))
		else:
			elem = self.doc.createElement(tag_name)
		#elem = xmlElement(self.doc,tag_name,**attributes)
		for attr_key in attributes:
			elem.setAttribute(attr_key, attributes[attr_key])
		return elem


class xmlDocument(object):

	def __init__(self):
		self.doc = xml.Document()
	
	# def createElement(self,tag_name,**attributes):
	# 	elem = self.doc.createElement(tag_name)
	# 	print elem.__class__
	# 	for attr_key in attributes:
	# 		elem.setAttribute(attr_key, attributes[attr_key])
	# 	return elem
	def appendChild(self,tag_name,**attributes):
		elem = self.createElement(tag_name,**attributes)
		self.doc.appendChild(elem.elem)
		return elem
	def createElement(self,tag_name,**attributes):
		return xmlElement(self.doc,tag_name,**attributes)

	def __getattr__(self,name):
		return getattr(self.doc,name)

class readhtmlCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		shopify 	= shopifyShop()
		prods 		= shopify.get("products")
		prod_types  = shopify.product_types()

		doc = xmlDocument()
		yml_catalog = doc.appendChild("yml_catalog",date=datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
		shop = yml_catalog.appendChild("shop")
		shop.appendChild("name","Set-and-app")
		shop.appendChild("company","Set-and-app")
		shop.appendChild("url","http://set-and-app.com/")
		shop.appendChild("platform","Shopify")
		shop.appendChild("version","1.0")
		shop.appendChild("agency","Set-and-app")
		shop.appendChild("email","set-and-app@yandex.ru")
		
		currencies = shop.appendChild("currencies")
		currencies.appendChild("currency",id="RUR",rate="1")

		categories = shop.appendChild("categories")
		for c in prod_types:
			#print c
			categories.appendChild("category",c,id=prod_types[c])

		offers = shop.appendChild("offers")
		for p in prods:
			if p["published_at"] == None:
				pass
			else:
				offer = offers.appendChild("offer",id=str(p["id"]),type="vendor.model",available="true")
				offer.appendChild("url"			,"http://set-and-app.com/products/%s" % p["handle"])
				offer.appendChild("price"		, p["variants"][0]["price"])
				offer.appendChild("currencyId"	, "RUR")
				offer.appendChild("categoryId"	, prod_types[p["product_type"]])
				for pic in p["images"]:
					offer.appendChild("picture"	, pic["src"].rstrip("?1722"))
				offer.appendChild("vendor"		, p["vendor"])
				offer.appendChild("model"		, p["title"])

		f = open(OUT_DIR + str(datetime.datetime.now().strftime("%Y%m%d%H%M%S") + ".yml"),'w+')
		#f.write(etree.tostring(root,"windows-1251"))
		f.write(doc.toprettyxml(encoding="windows-1251"))
		f.close()
		print "Файл YML успешно создан."
		#print doc.toprettyxml(indent="  ")
		#print doc.toxml()
